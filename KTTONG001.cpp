#include <bits/stdc++.h>

using namespace std;

int main()
{
    int test;
    cin >> test;
    while(test--)
    {
        int a, b, number_start = 0, number_end = 0;
        cin >> a >> b;

        if(a%2 == 1)
        {
            number_start = a;
        }
        else
        {
            number_start = a + 1;
        }

        if(b%2 == 1)
        {
            number_end = b;
        }
        else
        {
            number_end = b - 1;
        }

        long long khoang_cach = (number_end - number_start)/2 + 1;
        long long result = ((number_start + number_end)*khoang_cach)/2;
        
        cout << result << endl;
    }
}