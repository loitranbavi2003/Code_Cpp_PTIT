#include <bits/stdc++.h>

using namespace std;

int arr[1000] = {0};

void Output(int k);
void Try(int i, int n, int k);
long long Giai_thua(int n);

int main()
{
    int test;
    cin >> test;
    while(test--)
    {
        int n, k;
        cin >> n >> k;

        long long count = Giai_thua(n)/(Giai_thua(n - k)*Giai_thua(k));
        cout << count << endl;
        Try(1, n, k);
    }
}

void Try(int i, int n, int k)
{
    for(int t = arr[i - 1] + 1; t <= n - k + i; t++)
    {
        arr[i] = t;
        if(i==k) 
        {
            Output(k);
        }
        else 
        {
            Try(i+1, n, k);
        }
    }
}

long long Giai_thua(int n)
{
    if(n == 0)
    {
        return 1;
    }

    return n*Giai_thua(n - 1);
}

void Output(int k)
{
    cout << "[";
    for(int i = 1; i <= k; i++)
    {
        if(i < k)
        {
            cout << arr[i] << " ";
        }
        else
        {
            cout << arr[i];
        }
    }
    cout << "]";
    cout << endl;
}
