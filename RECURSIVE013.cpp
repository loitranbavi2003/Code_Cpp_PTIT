#include <bits/stdc++.h>

using namespace std;

int findMissingNumber(vector<int> &arr, int start, int end);

int main()
{
    int test;
    cin >> test;
    cin.ignore();
    while(test--)
    {
        string str;
        cin >> str;

        if(str == " ")
        {
            cout << "0" << endl;
        }
        else
        {
            vector<int> arr;
            stringstream ss(str);
            string temp;
            while(getline(ss, temp, ','))
            {
                arr.push_back(stoi(temp));
            }

            int result = findMissingNumber(arr, 0, arr.size() - 1);
            cout << result << endl;
        }
    }
}

int findMissingNumber(vector<int> &arr, int start, int end)
{
    if(arr.empty() || arr[0] > 0)
    {
        return 0;
    }

    if(start == end)
    {
        return start+1;
    }

    if(arr[start] != start)
    {
        return start;
    }

    return findMissingNumber(arr, start + 1, end);
}