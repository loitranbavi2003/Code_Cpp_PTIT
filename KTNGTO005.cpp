#include <bits/stdc++.h>

using namespace std;

long long SangNto(int n);

int main()
{
	int test;
	cin >> test;
	while(test--)
	{
		int n;
		cin >> n;
		
		long long sum = SangNto(n);
		cout << sum << endl;
	}
}

long long SangNto(int n)
{
	vector<int> primes(n+1);
	primes[0] = 0;
	primes[1] = 1;
	
	for(int i = 2; i <= n; i++)
	{
		primes[i] = i;
	}
	
	for(int i = 2; i <= n; i++)
	{
		if(primes[i] == i)
		{
			for(int j = i+i; j <= n; j += i)
			{
				primes[j] = i;
			}
		}
	}
	
	long long sum = 0;
	for(int i = 2; i <= n; i++)
	{
		sum += primes[i];
	}
	return sum;
}
