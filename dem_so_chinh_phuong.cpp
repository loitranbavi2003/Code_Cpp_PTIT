#include <bits/stdc++.h>

using namespace std;

int main() {
    int test;
    cin >> test;

    while (test--) {
        long long count = 0;
        long long a, b;
        cin >> a >> b;

        // Find the smallest perfect square larger than or equal to L
        // ceil(5.1) = 6
        long long smallestSquareRoot = ceil(sqrt(a));

        // Find the largest perfect square smaller than or equal to R
        // floor(3.7) = 3
        long long largestSquareRoot = floor(sqrt(b));

        // Count the number of perfect squares between L and R
        count = max(0LL, largestSquareRoot - smallestSquareRoot + 1);

        cout << count << endl;
    }

    return 0;
}
