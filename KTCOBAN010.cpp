#include <bits/stdc++.h>

using namespace std;

long long MOD = 1e9 + 7;

long long power(long long a, long long n);

int main()
{
    int test;
    cin >> test;
    while(test--)
    {
        long long a, n;
        cin >> a >> n;

        long long result = power(a, n);
        cout << result << endl;
    }
}

long long power(long long a, long long n)
{
    long long result = 1;
    while(n > 0)
    {
        if(n%2 == 1)
        {
            result = (result * a) % MOD;
        }

        a = (a * a) % MOD;
        n /= 2;
    }

    return result;
}