#include <bits/stdc++.h>

using namespace std;

int NumberLonely(vector<int> arr);

int main()
{
    int test;
    cin >> test;
    while(test--)
    {
        int n;
        cin >> n;

        vector<int> arr(n);
        for(int i = 0; i < n; i++)
        {
            cin >> arr[i];
        }

        int result = NumberLonely(arr);
        cout << result << endl;
    }
}

int NumberLonely(vector<int> arr)
{
    unordered_map<int, int> countvalue;
    for(int num : arr)
    {
        countvalue[num]++;
    }

    int number_lonely = 0;
    for(auto& i : countvalue)
    {
        if(i.second == 1)
        {
            number_lonely++;
        }
    }

    return number_lonely;
}