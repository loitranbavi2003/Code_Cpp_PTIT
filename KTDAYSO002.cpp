#include <bits/stdc++.h>

using namespace std;

int n, k, s;
int arr[30];
bool check[30];
int dem, sum;
vector<int> z;

void Try(int i)
{
    for(int j = i; j < n; j++)
    {
        if(check[j] == 0)
        {
            check[j] = 1;
            sum += arr[j];
            k--;
            if(k == 0)
            {
                z.push_back(sum);
            }
            else
            {
                Try(j+1);
            }
            sum -= arr[j];
            check[j] = 0;
            k++;
        }
    }
}

int main()
{
    int test;
    cin >> test;
    while(test--)
    {
        cin >> n >> k >> s;
        for(int i = 0; i < n; i++)
        {
            cin >> arr[i];
        }
        dem = 0;
        sum = 0;
        memset(check, 0, sizeof(check));

        Try(0);
        for(int i = 0; i < z.size(); i++)
        {
            if(s == z[i])
            {
                dem++;
            }
        }

        cout << dem << endl;
        z.clear();
    }
}