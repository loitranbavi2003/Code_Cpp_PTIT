#include <bits/stdc++.h>

using namespace std;

vector<int> Convert(string &str);
int CountStudents(vector<int> &students, vector<int> &sandwithes);

int main()
{
    int test;
    cin >> test;
    cin.ignore();
    while(test--)
    {
        string str_1, str_2;
        cin >> str_1 >> str_2;

        vector<int> students = Convert(str_1);
        vector<int> sandwithes = Convert(str_2);

        int result = CountStudents(students, sandwithes);
        cout << result << endl;
    }
}

int CountStudents(vector<int> &students, vector<int> &sandwithes)
{
    queue<int> students_queue;

    for(int i = 0; i < students.size(); i++)
    {
        students_queue.push(students[i]);
    }

    int count = 0;
    while(!students_queue.empty() && count <= sandwithes.size())
    {
        if(students_queue.front() == sandwithes.front())
        {
            students_queue.pop(); // loai bo ptu dau tien
            sandwithes.erase(sandwithes.begin()); // loai bo ptu dau tien
            count = 0;
        }
        else
        {
            students_queue.push(students_queue.front()); // chuyen gtri dau xuong cuoi
            students_queue.pop(); // xoa gtri dau tien
            count++;
        }
    }

    return students_queue.size();
}

vector<int> Convert(string &str)
{
    vector<int> arr;
    stringstream ss(str);
    string temp;
    while(getline(ss, temp, ','))
    {
        arr.push_back(stoi(temp));
    }

    return arr;
}