#include <iostream>
#include <vector>

using namespace std;

const int MOD = 29;

// Hàm tính a^b % mod
int modPow(int a, int b, int mod) {
    int result = 1;
    while (b > 0) {
        if (b % 2 == 1) {
            result = (result * a) % mod;
        }
        a = (a * a) % mod;
        b /= 2;
    }
    return result;
}

// Hàm tính tổng các ước dương của n^x cho mod
int sumOfDivisors(int n, int x, int mod) {
    int result = 0;
    for (int i = 0; i <= x; i++) {
        result = (result + modPow(n, i * x, mod)) % mod;
    }
    return result;
}

int main() {
    int numTestCases;
    cin >> numTestCases;

    while (numTestCases--) {
        int X;
        cin >> X;

        int S = sumOfDivisors(2004, X, MOD);
        cout << S << endl;
    }

    return 0;
}
