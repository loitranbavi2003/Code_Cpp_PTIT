#include <bits/stdc++.h>

using namespace std;

long long CountWay(int n);

int main()
{
	int test;
	cin >> test;
	while(test--)
	{
		int n;
		cin >> n;
		
		long long result = CountWay(n);
		cout << result << endl;
	}
}

long long CountWay(int n)
{
	vector<long long> dp(n+1, 0);
	dp[0] = 1;
	
	int i = 0;
	while(pow(2, i) <= n)
	{
		for(int j = pow(2, i); j <= n; j++)
		{
			dp[j] += dp[j - pow(2, i)];
		}
		i++;
	}
	
	return dp[n];
}
