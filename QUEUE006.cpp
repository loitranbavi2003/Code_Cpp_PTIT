#include <bits/stdc++.h>

using namespace std;

vector<int> ListNumberMax(vector<int> arr, int k);

int main()
{
    int test;
    cin >> test;
    while(test--)
    {
        string str;
        cin >> str;
        int k;
        cin >> k;

        vector<int> arr;
        stringstream ss(str);
        string temp;
        while(getline(ss, temp, ','))
        {
            arr.push_back(stoi(temp));
        }

        vector<int> result = ListNumberMax(arr, k);
        for(int i = 0; i < result.size(); i++)
        {
            cout << result[i];
            if(i < result.size() - 1)
            {
                cout << ",";
            }
        }
        cout << endl;
    }
}

vector<int> ListNumberMax(vector<int> arr, int k)
{
    std::vector<int> result;
    std::deque<int> window;

    for (int i = 0; i < arr.size(); ++i) {
        // Xóa các phần tử khỏi cửa sổ trượt khi chúng không còn nằm trong cửa sổ
        while (!window.empty() && window.front() < i - k + 1) {
            window.pop_front();
        }

        // Xóa các phần tử nhỏ hơn arr[i] từ cuối cửa sổ trượt
        while (!window.empty() && arr[window.back()] < arr[i]) {
            window.pop_back();
        }

        // Thêm vị trí hiện tại vào cửa sổ trượt
        window.push_back(i);

        // Lấy giá trị lớn nhất trong cửa sổ trượt khi đạt đến kích thước cửa sổ
        if (i >= k - 1) {
            result.push_back(arr[window.front()]);
        }
    }
    
    return result;
}