#include <bits/stdc++.h>

using namespace std;

int main()
{
	int test;
	cin >> test;
	while(test--)
	{
		int a, b;
		cin >> a >> b;
		
		int Min = min(a, b);
		int Max = max(a, b);
		
		a = Min;
		b = Max;
		
		int arr[10] = {0};
		
		for(int i = a; i <= b; i++)
		{
			int j = i;
			while(j > 0)
			{
				int temp = j%10;
				arr[temp]++;
				j /= 10;
			}
		}
		
		for(int i = 0; i < 9; i++)
		{
			cout << arr[i] << " ";
		}
		cout << arr[9] << endl;
	}
}

