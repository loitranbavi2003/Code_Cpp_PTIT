#include <iostream>
#include <vector>

using namespace std;

int main() {
    int t;
    cin >> t;

    while (t--) {
        int n;
        unsigned long long l, total = 0;
        cin >> n >> l;

        vector<long long> invitation(n - 1);
        for (int i = 0; i < n - 1; i++) {
            cin >> invitation[i];
            // Tính tổng của dãy số mời đã nhận
            total += invitation[i];
        }

        // Tính tổng của dãy số thực tế
        unsigned long long expected_total = n * (l + (l + n - 1)) / 2;

        // Tìm số giấy mời bị thiếu (số vắng mặt)
        unsigned long long missing_invitation = expected_total - total;

        cout << missing_invitation << endl;
    }

    return 0;
}
