#include <bits/stdc++.h>

using namespace std;

#define MAX 1000001

vector<int> primes;

void sieve();
int CountPrimeNumber(int a, int b);

int main()
{
    sieve();

    int test;
    cin >> test;
    while(test--)
    {
        int a, b;
        cin >> a >> b;

        cout << CountPrimeNumber(a, b) << endl;
    }
}

void sieve()
{
    vector<bool> isPrime(MAX, true);
    isPrime[0] = isPrime[1] = false;

    for(int i = 2; i * i <= MAX; i++)
    {
        if(isPrime[i])
        {
            for(int t = i*i; t <= MAX; t += i)
            {
                isPrime[t] = false;
            }
        }
    }

    for(int i = 2; i <= MAX; i++)
    {
        if(isPrime[i])
        {
            primes.push_back(i);
        }
    }
}
int CountPrimeNumber(int a, int b)
{
    int count = 0;
    for(int prime : primes)
    {
        if(prime >= a && prime <= b)
        {
            count++;
        }
    }

    return count;
}