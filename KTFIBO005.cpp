#include <bits/stdc++.h>

using namespace std;

long long Fibo(long long n);
int Count_Fibo(int n);

int main()
{
	int test;
	cin >> test;
	while(test--)
	{
		int n;
		cin >> n;
		
		int result = Count_Fibo(n);
		cout << result << endl;
	}
}

int Count_Fibo(int n)
{
	long long dp[n+1];
	memset(dp, 0, sizeof(dp));
	
	dp[0] = 1;
	int i = 1;
	long long fibo = Fibo(i);
	while(fibo <= n)
	{
		for(int j = n; j >= fibo; j--)
		{
			dp[j] += dp[j - fibo];
		}
		
		i++;
		fibo = Fibo(i);
	}
	
	return dp[n];
}

long long Fibo(long long n)
{
	if(n == 0 || n == 1)
	{
		return 1;
	}
	
	long long f0 = 1;
	long long f1 = 1;
	long long f2 = 0;
	for(int i = 2; i <= n; i++)
	{
		f2 = f0 + f1;
		f0 = f1;
		f1 = f2;
	}
	
	return f1;
}
