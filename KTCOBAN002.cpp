#include <bits/stdc++.h>

using namespace std;

void KTCOBAN(int k);
bool SquareNumber0_9(long long n);
bool SquareNumber(long long n);

int main()
{
    int test;
    cin >> test;
    while(test--)
    {
        int k;
        cin >> k;

        KTCOBAN(k);
    }
}

void KTCOBAN(int k)
{
    int flag = 0;
    unsigned long long n, m;
    n = pow(10, k-1);
    m = pow(10, k);
    // cout << n << " " << m << endl;

    for(long long i = n; i < m; i++)
    {
        if(SquareNumber(i))
        {
            if(SquareNumber0_9(i))
            {
                cout << i << endl;
                flag = 1;
                break;
            }
        }
    }

    if(flag == 0)
    {
        cout << "-1" << endl;
    }
}

bool SquareNumber0_9(long long n)
{
    long long a = 0;
    while(n != 0)
    {
        a = n%10;
        if(SquareNumber(a) == false)
        {
            return false;
        }

        n = n/10;
    }

    return true;
}

bool SquareNumber(long long n)
{
    if(n == 0 || n == 1 || n == 4 || n == 9)
    {
        return true;
    }

    long double x = 0;
    x = sqrt(n);

    if(x == (long long) x)
    {
        return true;
    }
    return false;
}