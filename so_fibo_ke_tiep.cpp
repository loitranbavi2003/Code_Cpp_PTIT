#include<iostream>

using namespace std;

int fibo(int n)
{
    if(n <= 3) return 4;

    int a{1}, b{2}, c{3};
    while(1)
    {
        a = b;
        b = c;
        c = a + b;

        if(c == n || c == n+1)
        {
            return c + 1;
        }
        else if (n < c)
        {
            return n + 1;
        }
    }
}

int main() 
{
    int t;
    cin >> t;
    while(t--) 
    {
        int n;
        cin >> n;
        cout << fibo(n) << endl;
    }
}