#include <bits/stdc++.h>

using namespace std;

bool isPrimeNumber(long long n);

int main()
{
    int test;
    cin >> test;
    while(test--)
    {
        long long n;
        cin >> n;

        int count = 0;
        for(int i = 2; i <= n/2; i++)
        {
            if(n%i == 0 && isPrimeNumber(i) == true)
            {
                count++;
            }
        }

        if(isPrimeNumber(n) == true)
        {
            count++;
        }

        cout << count << endl;
    }
}

bool isPrimeNumber(long long n)
{
    if(n < 2)
    {
        return false;
    }
    else if(n > 2)
    {
        if(n%2 == 0)
        {
            return false;
        }

        for(int i = 3; i <= sqrt(n); i+=2)
        {
            if(n%i == 0)
            {
                return false;
            }
        }
    }

    return true;
}