#include <bits/stdc++.h>

using namespace std;

bool Check_Nto(int n);
void Find_Nto(int P, int N, int S, vector<int> &current, vector<vector<int>> &result);

int main()
{
	int P, N, S;
	cin >> P >> N >> S;
	
	vector<int> current;
	vector<vector<int>> result;
	Find_Nto(P, N, S, current, result);
	
	if(result.empty())
	{
		cout << -1 << endl;
	}
	else
	{
		for(const auto& primeSet : result)
		{
			for(int prime : primeSet)
			{
				cout << prime << " ";
			}
			cout << endl;
		}
	}
}

void Find_Nto(int P, int N, int S, vector<int> &current, vector<vector<int>> &result)
{
	if(N == 0 & S == 0)
	{
		result.push_back(current);
		return;
	}
	
	if(N <= 0 || S <= 0)
	{
		return;
	}
	
	for(int i = P+1; i <= S; i++)
	{
		if(Check_Nto(i))
		{
			current.push_back(i);
			Find_Nto(i, N - 1, S - i, current, result);
			current.pop_back();
		}
	}
}

bool Check_Nto(int n)
{
	if(n < 2)
	{
		return false;
	}
	else if(n > 2)
	{
		if(n%2 == 0)
		{
			return false;
		}
		
		for(int i = 3; i <= sqrt(n); i += 2)
		{
			if(n%i == 0)
			{
				return false;
			}
		}
	}
	
	return true;
}
