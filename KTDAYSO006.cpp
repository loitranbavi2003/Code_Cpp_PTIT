#include <bits/stdc++.h>

using namespace std;

bool compare(string &a, string &b); // ham so sanh 2 chuoi
string TaoSoLon(vector<int> arr);

int main()
{
    int test;
    cin >> test;
    while(test--)
    {
        int n;
        cin >> n;

        vector<int> arr(n);
        for(int i = 0; i < n; i++)
        {
            cin >> arr[i];
        }

        string result = TaoSoLon(arr);
        cout << result << endl;
    }
}

string TaoSoLon(vector<int> arr)
{
    vector<string> str;

    for(int i = 0; i < arr.size(); i++)
    {
        str.push_back(to_string(arr[i]));
    } 

    sort(str.begin(), str.end(), compare);

    string result = "";
    for(int i = 0; i < str.size(); i++)
    {
        result += str[i];
    }

    return result;
}

bool compare(string &a, string &b)
{
    return a+b > b+a;
}