#include <bits/stdc++.h>

using namespace std;

bool Check_Nto(int n);
int Find_Nto(long long L, long long R);

int main()
{
	int test;
	cin >> test;
	while(test--)
	{
		long long L, R;
		cin >> L >> R;
		
		int result = Find_Nto(L, R);
		cout << result << endl;
	}
}

int Find_Nto(long long L, long long R)
{
	long long result = 0;
	for(int i = R; i >= L; i--)
	{
		if(Check_Nto(i) == true)
		{
			result = i;
			break;
		}
	}
	
	return result;
}

bool Check_Nto(int n)
{
	if(n < 2)
	{
		return false;
	}
	else if(n > 2)
	{
		if(n%2 == 0)
		{
			return false;
		}
		
		for(int i = 3; i <= sqrt(n); i += 2)
		{
			if(n%i == 0)
			{
				return false;
			}
		}
	}
	
	return true;
}
