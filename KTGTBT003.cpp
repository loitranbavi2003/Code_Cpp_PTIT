#include <bits/stdc++.h>

using namespace std;

void KTLT(int n);

int main()
{
    int test;
    cin >> test;
    while(test--)
    {
        int n;
        cin >> n;

        KTLT(n);
    }
}

void KTLT(int n)
{
    float i = 2, mu = 1, result = 1;
    unsigned long long factorial = 1;

    while(i <= n)
    {
        factorial = factorial*i;
        i++;
        mu = 1/i;
        result = factorial + result;
        result = pow(result, mu);
    }

    cout << fixed << setprecision(3) << result << " " << endl;
}
