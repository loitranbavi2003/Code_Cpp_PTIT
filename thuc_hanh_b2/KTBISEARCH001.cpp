#include<bits/stdc++.h>

using namespace std;

int binary_search(int a[],int n,int x)
{
    int l=0;
    int r=n-1;
    while(l<r)
	{
        int mid = (r+l)/2;
         if(a[mid]<x)
            l=mid+1;
        else
            r=mid;
    }
    if(a[l]==x)
    return l+1;
    else return 0;
}

int main()
{
    int n,m;
    cin>>n>>m;
    int a[100005],b[100005];
    for(int i=0;i<n;i++) cin>>a[i];
    
    for(int i=0;i<m;i++)
	{
        cin>>b[i];
    }
    
    for(int i=0;i<m;i++)
	{
    	if(i<m-1)
    	cout<<binary_search(a,n,b[i])<<" ";
    	else
    	cout<<binary_search(a,n,b[i]);
	}
	
    cout<<endl;
}
