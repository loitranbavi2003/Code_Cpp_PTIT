#include <iostream>
#include <vector>
using namespace std;

// H�m n�y ki?m tra xem m?t s? c� ph?i l� s? nguy�n t? hay kh�ng
bool isPrime(int num) {
    if (num < 2) return false;
    for (int i = 2; i * i <= num; ++i) {
        if (num % i == 0) return false;
    }
    return true;
}

// H�m n�y t?o ra d�y c�c s? nguy�n t? d?u ti�n
vector<int> generatePrimes(int n) {
    vector<int> primes;
    int num = 2;
    while (primes.size() < n) {
        if (isPrime(num)) {
            primes.push_back(num);
        }
        num++;
    }
    return primes;
}

int main() {
    int T;
    cin >> T;
    
    while (T--) {
        int n, k;
        cin >> n >> k;
        
        // T?o d�y s? nguy�n t? d?u ti�n
        vector<int> primes = generatePrimes(n);
        
        // X�y d?ng s? A(n) b?ng c�ch gh�p c�c s? nguy�n t? l?i
        int A_n = 0;
        for (int i = 0; i < n; ++i) {
            A_n = A_n * 10 + primes[i];
        }
        
        // T�m c�ch x�a k ch? s? ra kh?i s? A(n) sao cho s? c�n l?i l� l?n nh?t c� th?
        // B?t d?u t? v? tr� cu?i c�ng v� x�a c�c ch? s? kh�ng c?n thi?t
        for (int i = 0; i < k; ++i) {
            A_n /= 10;
        }
        
        cout << A_n << endl;
    }
    
    return 0;
}

