#include <iostream>
#include <vector>
#include <algorithm>
#include <string>

bool compare(std::string a, std::string b) 
{
    return a + b > b + a;
}

std::string findLargestNumber(std::vector<int>& nums) 
{
    std::vector<std::string> numsStr;
    
    for (int num : nums) 
	{
        numsStr.push_back(std::to_string(num));
    }
    
    std::sort(numsStr.begin(), numsStr.end(), compare);
    
    std::string result = "";
    for (std::string numStr : numsStr) 
	{
        result += numStr;
    }
    
    return result;
}

int main() 
{
    int t;
    std::cin >> t;
    
    while (t--) 
	{
        int n;
        std::cin >> n;
        
        std::vector<int> nums(n);
        for (int i = 0; i < n; i++) 
		{
            std::cin >> nums[i];
        }
        
        std::string largestNumber = findLargestNumber(nums);
        std::cout << largestNumber << std::endl;
    }
    
    return 0;
}
