#include<bits/stdc++.h>

using namespace std;

int dem(vector<int> a,int n)
{
    int count =0;
    vector<int> b(1000006,0);
    
    for(int i=0;i<n;i++)
	{
        b[a[i]]++;
    }
    
    for(int i=0;i<1000006;i++)
	{
        if(b[i]==1)
            count++;
    }
    return count;
}

int main()
{
    int t;
    cin>>t;
    while(t--)
	{
        int n;
        cin>>n;
        vector<int> a(n);
        for(int i=0;i<n;i++) cin>>a[i];
        cout<<dem(a,n)<<endl;
    }
}
