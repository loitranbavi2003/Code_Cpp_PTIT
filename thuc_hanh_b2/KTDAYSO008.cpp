#include <bits/stdc++.h>

using namespace std;

int main() 
{
    int t;
    cin >> t;
    
    while (t--) 
	{
        int N;
        cin >> N;
        vector<int> S(N);

        int totalSum = 0;
        for (int i = 0; i < N; i++) {
            cin >> S[i];
            totalSum += S[i];
        }

        vector<bool> dp(totalSum / 2 + 1, false);
        dp[0] = true;

        for (int i = 0; i < N; i++) 
		{
            for (int j = totalSum / 2; j >= S[i]; j--) 
			{
                dp[j] = dp[j] || dp[j - S[i]];
            }
        }

        int minDiff = totalSum;
        for (int j = totalSum / 2; j >= 0; j--)
		{
            if (dp[j]) 
			{
                minDiff = min(minDiff, totalSum - 2 * j);
            }
        }

        cout << minDiff << endl;
    }

    return 0;
}

