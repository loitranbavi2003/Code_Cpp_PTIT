#include <bits/stdc++.h>

using namespace std;

const int M = 1e9;
vector<int> fibo;
int Count;

void Khoi_Tao(void);
void CountWay(int n, int k, int index);

int main()
{
	int test;
	cin >> test;
	Khoi_Tao();
	
	while(test--)
	{
		int n, k;
		cin >> n >> k;
		
		Count = 0;
		CountWay(n, k, fibo.size()-1);
		cout << Count << endl;
	}
}

void CountWay(int n, int k, int index)
{
	if(n == 0 && k == 0)
	{
		Count++;
	}
	else
	{
		for(int i = index; i >= 0 && fibo[i]*k >= n; i--)
		{
			if(n >= fibo[i])
			{
				CountWay(n-fibo[i], k - 1, i);
			}
		}
	}
}

void Khoi_Tao(void)
{
	fibo.clear();
	fibo.push_back(1);
	fibo.push_back(2);
	int i = 2;
	while(fibo[i-1] + fibo[i-2] <= M)
	{
		fibo.push_back(fibo[i-1] + fibo[i-2]);
		i++;
	}
}
