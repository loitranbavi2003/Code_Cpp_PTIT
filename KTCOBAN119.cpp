#include <bits/stdc++.h>

using namespace std;

int CountZero(int n);

int main()
{
	int test;
	cin >> test;
	while(test--)
	{
		int n, k;
		cin >> n >> k;
		
		int count = 0;
		for(int i = 0; i <= n; i++)
		{
			if(CountZero(i) == k)
			{
				count++;
			}
		}
		cout << count << endl;
	}
}

int CountZero(int n)
{
	if(n == 0)
	{
		return 1;
	}
	
	int count = 0;
	while(n > 0)
	{
		if(n%2 == 0)
		{
			count++;
		}
		n /= 2;
	}
	
	return count;
}
