#include <bits/stdc++.h>

using namespace std;

int ucln(int a, int b);

int main()
{
    int test;
    cin >> test;
    while(test--)
    {
        int a, b;
        cin >> a >> b;

        int result = ucln(a, b);
        cout << result << endl;
    }
}

int ucln(int a, int b)
{
    if(a == 0 || b == 0)
    {
        return a + b;
    }

    while(a != b)
    {
        if(a > b)
        {
            a -= b;
        }
        
        if(b > a)
        {
            b -= a;
        }
    }

    return a;
}