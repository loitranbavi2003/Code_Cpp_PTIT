#include <bits/stdc++.h>

using namespace std;

string BuildString(string &str);

int main()
{
    int test;
    cin >> test;
    cin.ignore();
    while(test--)
    {
        string str;
        cin >> str;

        string result = BuildString(str);
        cout << result << endl;
    }
}

string BuildString(string &str)
{
    stack<int> s;
    vector<int> arr(str.length()+1);
    int count = 1;

    for(int i = 0; i <= str.length(); i++)
    {
        s.push(i);
        if(i == str.length() || str[i] == 'I')
        {
            while(!s.empty())
            {
                arr[s.top()] = count++;
                s.pop();
            }
        }
    }

    string result;
    for(int i = 0; i < arr.size(); i++)
    {
        result += to_string(arr[i]);
    }
    
    return result;
}