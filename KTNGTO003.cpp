#include <bits/stdc++.h>

using namespace std;

bool CheckPrime(long long n);
long long SumPrime(long long n);

int main()
{
    int test;
    cin >> test;
    while(test--)
    {
        long long n;
        cin >> n;

        cout << SumPrime(n) << endl;
    }
}

long long SumPrime(long long n)
{
    if(n<=4)
    {
        return 5;
    }

    if(n%2==0)
    {
        n++;
    }

    while(!CheckPrime(n) || !CheckPrime(n-2))
    {
        n=n+2;
    }
    return n;
}

bool CheckPrime(long long n)
{
    if(n == 1)
    {
        return false;
    }

    if(n == 2 || n == 3)
    {
        return true;
    }

    if(n%2 == 0 || n%3 == 0)
    {
        return false;
    }

    for(int i = 5; i <= sqrt(n); i+= 6)
    {
        if((n%i == 0) || n%(i+2) == 0)
        {
            return false;
        }
    }
    return true;
}
