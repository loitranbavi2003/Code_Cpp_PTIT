#include <bits/stdc++.h>

using namespace std;

long long Fibo(long long n);

int main()
{
	int test;
	cin >> test;
	while(test--)
	{
		long long n;
		cin >> n;
		
		long long sum = 0;
		for(int i = 2; i <= 50; i++)
		{
			if(n >= Fibo(i) && Fibo(i)%2 == 0)
			{
				sum += Fibo(i);
			}
		}
		
		cout << sum << endl;
	}
}

long long Fibo(long long n)
{
	if(n == 0 || n == 1)
	{
		return 1;
	}
	
	long long f0 = 1;
	long long f1 = 1;
	long long f2 = 0;
	for(int i = 2; i <= n; i++)
	{
		f2 = f0 + f1;
		f0 = f1;
		f1 = f2;
	}
	
	return f1;
}
