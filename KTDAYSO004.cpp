#include <bits/stdc++.h>

using namespace std;

int main()
{
    int test;
    cin >> test;
    while(test--)
    {
        int n;
        cin >> n;

        vector<int> arr(n);
        vector<int> dp(n, 1);

        for(int i = 0; i < n; i++)
        {
            cin >> arr[i];
        }

        int max_len = 1;
        for(int i = 1; i < n; i++)
        {
            for(int j = 0; j < i; j++)
            {
                if(arr[i] > arr[j])
                {
                    dp[i] = max(dp[i], dp[j] + 1);
                }
            }

            max_len = max(max_len, dp[i]);
        }

        cout << max_len << endl;
    }
}