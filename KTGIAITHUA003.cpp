#include <bits/stdc++.h>

using namespace std;

int main() 
{
    int t; 
    cin >>t;
    while(t--)
    {
        long long n;
        cin >> n;

        long long ans = 0;
        int k = 1;

        while(pow(5,k) <= n)
        {
            ans += n/(pow(5,k));
            k++;
        }
        cout << ans << endl;
    }
    return 0;
}
