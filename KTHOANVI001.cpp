#include <bits/stdc++.h>

using namespace std;

int arr[11] = {0}, check[11] = {0};

void Out(int n);
void Try(int i, int n);

int main()
{
    int test;
    cin >> test;
    while(test--)
    {
        int n;
        cin >> n;

        Try(1, n);
    }
}

void Try(int i, int n)
{
    for(int j = 1; j <= n; j++)
    {
        if(check[j] == 0)
        {
            check[j] = 1;
            arr[i] = j;

            if(i == n)
            {
                Out(n);
            }
            else
            {
                Try(i+1, n);
            }

            check[j] = 0;
        }
    }
}

void Out(int n)
{
    for(int i = 1; i < n; i++)
    {
        cout << arr[i] << " ";
    }
    cout << arr[n] << endl;
}