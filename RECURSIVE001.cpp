#include <bits/stdc++.h>

using namespace std;

int main()
{
    int test;
    cin >> test;
    cin.ignore();
    while(test--)
    {
        string str;
        getline(cin, str);

        vector<int> arr;
        stringstream ss(str);
        string temp;
        while(getline(ss, temp, ','))
        {
            arr.push_back(stoi(temp));
        }

        int sum = 0;
        for(int i = 0; i < arr.size(); i++)
        {
            sum += arr[i];
        }
        cout << sum << endl;
    }
}