#include <bits/stdc++.h>

using namespace std;

bool NgoacHopLe(string &str);
string XoaNgoac(string &str);

int main()
{
    int test;
    cin >> test;
    cin.ignore();
    while(test--)
    {
        string str;
        getline(cin, str);

        string result = XoaNgoac(str);
        cout << result << endl;
    }
}

bool NgoacHopLe(string &str)
{
    stack<char> s;

    for(int i = 0; i < str.length(); i++)
    {
        if(str[i] == '(' || str[i] == '[' || str[i] == '{')
        {
            s.push(str[i]);
        }
        else
        {
            char top = s.top();
            s.pop();
            if(str[i] == ')' && top != '('
                || str[i] == ']' && top != '['
                || str[i] == '}' && top != '{')
            {
                return false;
            }
        }
    }

    return s.empty();
}

string XoaNgoac(string &str)
{
    vector<string> phanda;
    string temp = "";
    for(int i = 0; i < str.length(); i++)
    {
        temp += str[i];

        if(NgoacHopLe(temp))
        {
            phanda.push_back(temp);
            temp = "";
        }
    }

    string result = "";
    for(int i = 0 ; i < phanda.size(); i++)
    {
        result += phanda[i].substr(1, phanda[i].length() - 2);
    }

    return result;
}
