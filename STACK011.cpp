#include <bits/stdc++.h>

using namespace std;

string ReverseString(string &str);

int main()
{
    int test;
    cin >> test;
    cin.ignore();
    while(test--)
    {
        string str;
        cin >> str;

        string result = ReverseString(str);
        cout << result << endl;
    }
}

string ReverseString(string &str)
{
    stack<char> s;

    for(int i = 0; i < str.length(); i++)
    {
        if(str[i] == ')')
        {
            // dao nguoc chuoi trong ngoac
            string temp;
            while(!s.empty() && s.top() != '(')
            {
                temp += s.top();
                s.pop();
            }
            s.pop(); // loai bo ki tu (
            
            // dua chuoi da dao nguoc lai stack
            for(int i = 0; i < temp.length(); i++)
            {
                s.push(temp[i]);
            }
        }
        else
        {
            s.push(str[i]);
        }
    }

    string result;
    while(!s.empty())
    {
        result = s.top() + result;
        s.pop();
    }

    return result;
}