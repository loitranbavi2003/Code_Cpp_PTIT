#include <bits/stdc++.h>

using namespace std;

int main()
{
    int test;
    cin >> test;
    while(test--)
    {
        long n, total = 0;
        cin >> n;

        for(int i = 1; i <= n/2; i++)
        {
            if(n%i == 0)
            {
                total += i;
            }
        }

        cout << total << endl;
    }
}
