#include <bits/stdc++.h>

using namespace std;

long long Number_Catalan(int n);

int main()
{
    int test;
    cin >> test;
    while(test--)
    {
        int n;
        cin >> n;

        long long catalan = Number_Catalan(n);
        cout << catalan << endl;
    }
}

long long Number_Catalan(int n)
{
    if(n == 1)
    {
        return 1;
    }

    long long catalan[n+1];
    memset(catalan, 0, sizeof(catalan));
    catalan[0] = 1;

    for(int i = 0; i < n; i++)
    {
        for(int j = 0; j <= i; j++)
        {
            catalan[i+1] += catalan[j] * catalan[i - j];
        }
    }

    return catalan[n];
}