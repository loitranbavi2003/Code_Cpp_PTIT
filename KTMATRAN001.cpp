#include <bits/stdc++.h>

using namespace std;

int main()
{
    int test;
    cin >> test;
    while(test--)
    {
        int n, m;
        cin >> n >> m;

        long long arr[n][m];
        for(int i = 0; i < n; i ++)
        {
            for(int j = 0; j < m; j++)
            {
                cin >> arr[i][j];
            }
        }

        if(m == 1)
        {
            long long result = 0;
            for(int i = 0; i < n; i++)
            {
                result += arr[i][0];
            }
            cout << result << endl;
        }
        else
        {
            long long k = 0;
            for(int i = 1; i < n; i++)
            {
                for(int j = 0; j < m; j++)
                {
                    if(j == 0)
                    {
                        k = max(arr[i-1][j], arr[i-1][j+1]);
                    }
                    else if(j == m - 1)
                    {
                        k = max(arr[i-1][j], arr[i-1][j-1]);
                    }
                    else
                    {
                        k = max(arr[i-1][j], max(arr[i-1][j+1], arr[i-1][j-1]));
                    }

                    arr[i][j] += k;
                }
            }

            long long maximum = arr[n-1][0];
            for(int i = 1; i < m; i++)
            {
                maximum = max(maximum, arr[n-1][i]);
            }

            cout << maximum << endl;
        }
    }
}
