#include <bits/stdc++.h>

using namespace std;

vector<string> build(vector<int> &arr, int n);

int main()
{
    int test;
    cin >> test;
    cin.ignore();
    while(test--)
    {
        string str;
        cin >> str;
        int n;
        cin >> n;
        cin.ignore();

        vector<int> arr;
        stringstream ss(str);
        string temp;
        while(getline(ss, temp, ','))
        {
            arr.push_back(stoi(temp));
        }

        vector<string> result = build(arr, n);
        for(int i = 0; i < result.size(); i++)
        {
            cout << result[i];
            if(i < result.size() - 1)
            {
                cout << ", ";
            }
        }
        cout << endl;
    }
}

vector<string> build(vector<int> &arr, int n)
{
    vector<string> result;
    stack<int> s;

    int count = 0;
    for(int i = 1; i <= n; i++)
    {
        if(!s.empty() && s.top() != arr[count])
        {
            s.pop();
            result.push_back("Pop");
            count++;
        }
        else
        {
            s.push(i);
            result.push_back("Push");
        }
    }

    return result;
}
