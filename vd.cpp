#include <bits/stdc++.h>

using namespace std;

int Find(string &child, string &parent);

int main()
{
    int test;
    cin >> test;
    cin.ignore();
    while(test--)
    {
        string str;
        getline(cin, str);

        string child, parent;
        int length = str.length(), count = 0;
        for(int i = 0; i < length; i++)
        {
            if(str[i] == '"')
            {
                count++;
            }

            if(count == 1 && str[i] != '"')
            {
                child += str[i];
            }
            else if(count == 3 && str[i] != '"')
            {
                parent += str[i];
            }
        }

        int result = Find(child, parent);
        cout << result << endl;
    }
}

int Find(string &child, string &parent)
{
    if(child.empty())
    {
        return 0;
    }

    if(parent.find(child) >= 1 || parent.find(child) == 0)
    {
    	return parent.find(child);
	}

    return -1;
}
