#include <bits/stdc++.h>

using namespace std;

int DayConMAX(vector<int> arr);

int main()
{
    int test;
    cin >> test;
    while(test--)
    {
        int n;
        cin >> n;

        vector<int> arr(n);

        for(int i = 0; i < n; i++)
        {
            cin >> arr[i];
        }

        cout << DayConMAX(arr) << endl;
    }
}

int DayConMAX(vector<int> arr)
{
    int max_sum = arr[0];
    int current_max = arr[0];

    for(int i = 1; i < arr.size(); i++)
    {
        current_max = max(arr[i], current_max + arr[i]);
        max_sum = max(max_sum, current_max);
    }

    return max_sum;
}