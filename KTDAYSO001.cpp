#include <bits/stdc++.h>

using namespace std;

int main()
{
    int test;
    cin >> test;
    while(test--)
    {
        int n;
        cin >> n;

        long long arr[n];
        for(int i = 0; i < n; i++)
        {
            cin >> arr[i];
        }
        if(n <= 2)
        {
            cout << "YES" << endl;
        }
        else
        {
            int X = arr[1] - arr[0], flag = 0;

            for(int i = 1; i < n-1; i++)
            {
                if((arr[i+1] - arr[i]) == X)
                {
                    flag = 1;
                }
                else
                {
                    flag = 0;
                    break;
                }
            }

            if(flag == 1)
            {
                cout << "YES" << endl;
            }
            else
            {
                cout << "NO" << endl;
            }
        }
    }
}