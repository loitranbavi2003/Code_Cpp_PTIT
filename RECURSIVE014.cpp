#include <iostream>
#include <vector>
#include <climits>

using namespace std;

// Hàm đệ quy để tính số bước tối thiểu
int minSteps(int n, vector<int>& dp) {
    // Trường hợp cơ bản
    if (n == 1) {
        return 0;
    }

    // Nếu đã tính toán trước đó, trả về kết quả đã lưu
    if (dp[n] != -1) {
        return dp[n];
    }

    // Tính số bước tối thiểu bằng cách thử các phép toán và chọn phép toán tối ưu
    int steps = 1 + minSteps(n - 1, dp);

    if (n % 2 == 0) {
        steps = min(steps, 1 + minSteps(n / 2, dp));
    }

    if (n % 3 == 0) {
        steps = min(steps, 1 + minSteps(n / 3, dp));
    }

    // Lưu kết quả vào dp và trả về
    dp[n] = steps;
    return steps;
}

int main() {
    int t;
    cin >> t;

    for (int i = 0; i < t; ++i) {
        int n;
        cin >> n;

        // Khởi tạo mảng dp chứa kết quả đã tính toán
        vector<int> dp(n + 1, -1);

        // Gọi hàm đệ quy và in kết quả
        cout << minSteps(n, dp) << endl;
    }

    return 0;
}
