#include <iostream>
#include <string>

using namespace std;

// Hàm chuyển đổi từ hệ thập phân sang hệ cơ số 16 (hexa)
string decimalToHexadecimal(long long decimal) 
{
    if (decimal == 0) 
    {
        return "0";
    }

    string hexadecimal = "";
    while (decimal > 0) 
    {
        int remainder = decimal % 16;
        if (remainder < 10) 
        {
            hexadecimal = char('0' + remainder) + hexadecimal;
        } 
        else 
        {
            hexadecimal = char('A' + remainder - 10) + hexadecimal;
        }
        decimal /= 16;
    }
    return hexadecimal;
}

int main() {
    int T;
    cin >> T;
    while(T--)
    {
        long long n;
        cin >> n;
        string hexadecimal_n = decimalToHexadecimal(n);
        cout << hexadecimal_n << endl;
    }

    return 0;
}
