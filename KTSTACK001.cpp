#include <bits/stdc++.h>

using namespace std;

bool Check(string &str);

int main()
{
	int test;
	cin >> test;
	while(test--)
	{
		string str;
		cin >> str;
		
		if(Check(str))
		{
			cout << "YES" << endl;
		}
		else
		{
			cout << "NO" << endl;
		}
	}
}

bool Check(string &str)
{
	stack<char> s;
	
	for(int i = 0; i < str.size(); i++)
	{
		if(str[i] == '[' || str[i] == '(' || str[i] == '{')
		{
			s.push(str[i]);
		}
		else
		{
			char top = s.top();
			if((str[i] == ']' && top == '[')
				|| (str[i] == ')' && top == '(')
				|| (str[i] == '}' && top == '{'))
			{
				s.pop();
			}
		}
	}
	
	return s.empty();
}
