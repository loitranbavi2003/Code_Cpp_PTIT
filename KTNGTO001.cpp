#include <bits/stdc++.h>

using namespace std;

bool isPrimeNumber(int n);
bool isPerfectNumber(int n);

int main()
{
    int test;
    cin >> test;
    while(test--)
    {
        int n, count = 0;
        cin >> n;

        if(n == 1)
        {
            cout << "0" << endl;
        }
        else
        {
            for(int i = 2; i <= n; i++)
            {
                if(isPerfectNumber(i))
                {
                    count++;
                }
            }

            cout << count << endl;
        }
    }
}

bool isPerfectNumber(int n)
{
    if(isPrimeNumber(n) == false)
    {
        return false;
    }

    while(n != 0)
    {
        if(isPrimeNumber(n%10) == false)
        {
            return false;
        }
        n /= 10;
    }
    return true;
}

bool isPrimeNumber(int n)
{
    if(n < 2)
    {
        return false;
    }
    else if(n > 2)
    {
        if(n%2 == 0)
        {
            return false;
        }

        for(int i = 3; i <= sqrt(n); i+=2)
        {
            if(n%i == 0)
            {
                return false;
            }
        }
    }

    return true;
}
