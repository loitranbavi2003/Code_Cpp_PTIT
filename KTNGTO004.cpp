#include <bits/stdc++.h>

using namespace std;

bool CheckNto(long long n);
long long Check(long long n);

int main()
{
	int test;
	cin >> test;
	while(test--)
	{
		long long n;
		cin >> n;
		
		long long result = Check(n);
		cout << result << endl;
	}
}

long long Check(long long n)
{
	if(CheckNto(n))
	{
		return n;
	}
	
	for(long long i = 2; i <= n/i; i++)
	{
		if(n%i==0)
		{
			while(n%i==0)
			{
				n /= i;
				if(CheckNto(n))
				{
					return n;
				}
			}
		}
	}
}

bool CheckNto(long long n)
{
	if(n < 2)
	{
		return false;
	}
	else if(n > 2)
	{
		if(n%2 == 0)
		{
			return false;
		}
		
		for(int i = 3; i <= sqrt(n); i += 2)
		{
			if(n%i == 0)
			{
				return false;
			}
		}
	}
	
	return true;
}
