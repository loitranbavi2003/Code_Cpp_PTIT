#include "bits/stdc++.h"

using namespace std;

long long giaithua(int n);

int main()
{
    int test;
    cin >> test;
    while(test--)
    {
        int n;
        cin >> n;

        long long result = giaithua(n);
        cout << result << endl;
    }
}

long long giaithua(int n)
{
    if(n == 1)
    {
        return 1;
    }

    return n*giaithua(n-1);
}
