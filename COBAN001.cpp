#include <bits/stdc++.h>

using namespace std;

long long giai_thua(int x, long long mu);

int main()
{
    int test;
    cin >> test;
    while(test--)
    {
        long long n, mu = 0, i = 0, sum = 0;
        cin >> n;
        string len = to_string(n);
        mu = len.length();

        while(n != 0)
        {
            sum = sum + giai_thua(n%10, mu);
            n = n/10;
            i++;
        }

        if(sum == stoi(len))
        {
            cout << "1" << endl;
        }
        else
        {
            cout << "0" << endl;
        }
    }
}

long long giai_thua(int x, long long mu)
{
    long long tich = 1;

    while(mu--)
    {
        tich *= x;
    }

    return tich;
}
