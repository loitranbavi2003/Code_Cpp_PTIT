#include <bits/stdc++.h>

using namespace std;

stack<char> deleteString(string &str);

int main()
{
    int test;
    cin >> test;
    cin.ignore();
    while(test--)
    {
        string str_1, str_2;
        cin >> str_1 >> str_2;

        stack<char> result_1 = deleteString(str_1);
        stack<char> result_2 = deleteString(str_2);

        if(result_1 == result_2)
        {
            cout << 1 << endl;
        }
        else
        {
            cout << 0 << endl;
        }
    }
}

stack<char> deleteString(string &str)
{
    stack<char> s;

    for(int i = 0; i < str.length(); i++)
    {
        if(str[i] == '#')
        {
            if(!s.empty())
            {
                s.pop();
            }
        }
        else
        {
            s.push(str[i]);
        }
    }

    return s;
}