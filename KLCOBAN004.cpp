#include <bits/stdc++.h>

using namespace std;

int main()
{
    int test;
    cin >> test;
    while(test--)
    {
        int n, mu = 0;
        long long result = 0;
        cin >> n;

        if(n%2 == 0)
        {
            mu = n/2;
            result = 9*pow(10, mu - 1);
            cout << result << endl;
        }
        else 
        {
            mu = n/2;
            result = 9*pow(10, mu);
            cout << result << endl;
        }
    }
}