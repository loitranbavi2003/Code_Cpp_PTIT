#include <bits/stdc++.h>

using namespace std;

string File_Excel(long long n);

int main()
{
	std::ifstring inputFile("input.txt");
	std::ofstring outputFile("output.txt");
	
	long long n;
	while(inputFile >> n)
	{
		string str = File_Excel(n);
		outputFile << str << endl;
	}
}

string File_Excel(long long n)
{
	string str = "";
	while(n > 0)
	{
		long long x = (n-1)%26;
		str = char('A' + x) + str;
		n = (n-1)/26;
	}
	
	return str;
}
