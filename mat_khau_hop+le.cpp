#include <bits/stdc++.h>
#include <string>

using namespace std;

std::ifstream inputFile("password.in");
std::ofstream outputFile("res.out");

bool isSpecialChar(char c)
{
    return (ispunct(c));
}

bool isValidPassword(std::string &password)
{
    bool flag_inhoa = false;
    bool flag_inthuong = false;
    bool flag_so = false;
    bool flag_kitu = false;
    char prevChar = '\0';

    if (password.length() < 8)
    {
        return false;
    }

    for (char c : password)
    {
        if (std::islower(c))
        {
            flag_inthuong = true;
        }
        else if (std::isupper(c))
        {
            flag_inhoa = true;
        }
        else if (std::isdigit(c))
        {
            flag_so = true;
        }
        else if (isSpecialChar(c))
        {
            flag_kitu = true;
        }

        if(c == prevChar)
        {
            return false;
        }
        prevChar = c;
    }

    return flag_inhoa && flag_inthuong && flag_so && flag_kitu;
}

int main()
{
    std::string password;
    
    while(std::getline(inputFile, password))
    {
        if(isValidPassword(password))
        {
            outputFile << "VALID" << std::endl;
        }
        else
        {
            outputFile << "INVALID" << std::endl;
        }
    }

    inputFile.close();
    outputFile.close();
}