#include <bits/stdc++.h>

using namespace std;

int numberOne(long n);

int main()
{
    int test;
    cin >> test;
    while(test--)
    {
        long n;
        cin >> n;

        int sum = 0;
        for(long i = 1; i <= n; i++)
        {
            if(numberOne(i))
            {
                sum += numberOne(i);
            }
        }

        cout << sum << endl;
    }
}

int numberOne(long n)
{
    int count = 0;
    while(n != 0)
    {
        if(n%10 == 1)
        {
            count++;
        }

        n /= 10;
    }

    return count;
}
