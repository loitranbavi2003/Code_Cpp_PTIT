#include <bits/stdc++.h>

using namespace std;

long long GiaiThuaXauXi(int n);

int main()
{
    int test;
    cin >> test;
    while(test--)
    {
        int n;
        cin >> n;

        long long result = GiaiThuaXauXi(n);
        cout << result << endl;
    }
}

long long GiaiThuaXauXi(int n)
{
    stack<int> s;
    s.push(n);

    int count = 0;
    for(int i = n-1; i >= 1; i--)
    {
        if(count%4 == 0)
        {
            s.top() *= i;
        }
        else if(count%4 == 1)
        {
            s.top() /= i;
        }
        else if(count%4 == 2)
        {
            s.push(i);
        }
        else
        {
            s.push(-i);
        }
        count++;
    }

    long long result = 0;
    while(!s.empty())
    {
        result += s.top();
        s.pop();
    }

    return result;
}