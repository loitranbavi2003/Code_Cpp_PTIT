#include <bits/stdc++.h>

using namespace std;

string DonGianPath(string &str);

int main()
{
    int test;
    cin >> test;
    cin.ignore();
    while(test--)
    {
        string str;
        cin >> str;

        string result = DonGianPath(str);
        cout << result << endl;
    }
}

string DonGianPath(string &str)
{
    stack<string> s;
    stringstream ss(str);
    string temp;
    while(getline(ss, temp, '/'))
    {
        if(temp == "" || temp == ".")
        {

        }
        else if(temp == "..")
        {
            if(!s.empty())
            {
                s.pop();
            }
        }
        else
        {
            s.push(temp);
        }
    }

    vector<string> arr;
    while(!s.empty())
    {
        arr.push_back(s.top());
        s.pop();
    }

    string result;
    for(int i = 0 ; i < arr.size(); i++)
    {
        result = "/" + arr[i] + result;
    }

    if(result.empty())
    {
        result = "/";
    }
    return result;
}