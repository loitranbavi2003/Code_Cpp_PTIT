#include <bits/stdc++.h>

using namespace std;

std::vector<int> dailyTemperatures(const std::vector<int>& temperatures) 
{
    int n = temperatures.size();
    std::vector<int> result(n, 0);
    std::stack<int> s;
    
    for (int i = 0; i < n; i++) 
    {
        while(!s.empty() && temperatures[i] > temperatures[s.top()]) 
        {
            cout << i << " ";
            int index = s.top();
            cout << index << " ";
            s.pop();
            result[index] = i - index;
            cout << result[index] << " " << endl;
        }
        s.push(i);
        cout << s.top() << endl;
    }

    return result;
}

int main()
{
    int test;
    cin >> test;
    cin.ignore();
    while(test--)
    {
        string str;
        cin >> str;

        vector<int> arr_temp;
        stringstream ss(str);
        string temp;
        while(getline(ss, temp, ','))
        {
            arr_temp.push_back(stoi(temp));
        }

        vector<int> answer = dailyTemperatures(arr_temp);

        // for(int i = 0; i < answer.size(); i++)
        // {
        //     cout << answer[i];
        //     if(i < answer.size() - 1)
        //     {
        //         cout << ",";
        //     }
        // }
        cout << endl;
    }
}