#include <bits/stdc++.h>

using namespace std;

long long Fibo(int n)
{
	if(n == 0 || n == 1)
	{
		return 1;
	}
	
	long long f0 = 1;
	long long f1 = 1;
	long long f2 = 0;
	for(long long i = 2; i <= n; i++)
	{
		f2 = f0 + f1;
		f0 = f1;
		f1 = f2;
	}
	
	return f1;
}

int main()
{
	int test;
	cin >> test;
	while(test--)
	{
		int n;
		cin >> n;
		
		long long nonFibo = 0, i = 3;
		long long fibo1 = Fibo(i), fibo2 = Fibo(i+1);
		while(fibo2 - fibo1 - 1 < n)
		{
			i++;
			n = n - (fibo2 - fibo1 - 1);
			fibo1 = Fibo(i);
			fibo2 = Fibo(i+1);
		}
		nonFibo = fibo1 + n;
		cout << nonFibo << endl;
	}
}
