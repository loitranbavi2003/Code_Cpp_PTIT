#include <bits/stdc++.h>

using namespace std;

vector<int> PricesEndStore(vector<int> &prices);

int main()
{
    int test;
    cin >> test;
    cin.ignore();
    while(test--)
    {
        string str;
        cin >> str;

        vector<int> prices;
        stringstream ss(str);
        string temp;
        while(getline(ss, temp, ','))
        {
            prices.push_back(stoi(temp));
        }

        vector<int> result = PricesEndStore(prices);
        for(int i = 0; i < result.size(); i++)
        {
            cout << result[i];
            if(i < result.size() - 1)
            {
                cout << ",";
            }
        }
        cout << endl;
    }
}

vector<int> PricesEndStore(vector<int> &prices)
{
    stack<int> s;

    for(int i = 0; i < prices.size(); i++)
    {
        while(!s.empty() && prices[s.top()] >= prices[i])
        {
            prices[s.top()] -= prices[i];
            s.pop();
        }
        s.push(i);
    }

    return prices;
}