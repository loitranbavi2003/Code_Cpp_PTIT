#include <bits/stdc++.h>

using namespace std;

string DeleteStar(string &str);

int main()
{
    int test;
    cin >> test;
    cin.ignore();
    while(test--)
    {
        string str;
        cin >> str;

        string result = DeleteStar(str);
        cout << result << endl;
    }
}

string DeleteStar(string &str)
{
    stack<char> s;

    for(int i = 0; i < str.length(); i++)
    {
        if(!s.empty() && (s.top() != '*' && str[i] == '*'))
        {
            s.pop();
        }
        else
        {
            s.push(str[i]);
        }
    }

    string result;
    while(!s.empty())
    {
        result = s.top() + result;
        s.pop();
    }

    return result;
}