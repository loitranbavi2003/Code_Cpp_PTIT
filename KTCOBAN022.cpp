#include <bits/stdc++.h>

using namespace std;

int main()
{
    int test;
    cin >> test;
    while(test--)
    {
        int x, y, m;
        cin >> x >> y >> m;

        long long hopmuabangtien = 0, hopdoibangqua = 0, tongsohop = 0;

        hopmuabangtien = m/x; // mua dc bn cai
        tongsohop = hopmuabangtien;

        while(hopmuabangtien >= y)
        {
            hopdoibangqua = hopmuabangtien/y;
            tongsohop += hopdoibangqua;
            hopmuabangtien = hopmuabangtien%y + hopdoibangqua;
        }

        cout << tongsohop << endl;
    }
}