#include <bits/stdc++.h>

using namespace std;

int doi_xung(long long n);
void doi_xung_gan_nhat(long long number);

int main()
{
    int test;
    cin >> test;
    while(test--)
    {
        long long number;
        cin >> number;

        if(doi_xung(number) == 1)
        {
            cout << number << endl;
        }
        else
        {
            doi_xung_gan_nhat(number);
        }
    }
}

void doi_xung_gan_nhat(long long number)
{
    long long number_big, number_small;

    number_big = number;
    number_small = number;

    while(doi_xung(number_small) == 0)
    {
        number_small--;
    }

    while(doi_xung(number_big) == 0)
    {
        number_big++;
    }

    if(abs(number - number_small) > abs(number - number_big))
    {
        cout << number_big << endl;
    }
    else if(abs(number - number_small) < abs(number - number_big))
    {
        cout << number_small << endl;
    }
    else
    {
        cout << number_small << " " << number_big << endl;
    }
}

int doi_xung(long long n)
{
    int flag = 1;
    string str;
    str = to_string(n);

    for(int i = 0; i <= (str.length())/2; i++)
    {
        if(str[i] != str[str.length() - i-1])
        {
            flag = 0;
            break;
        }
    }

    return flag;
}