#include <bits/stdc++.h>

using namespace std;

int main()
{
    int test;
    cin >> test;
    cin.ignore();
    while(test--)
    {
        string str;
        cin >> str;

        int max_diff = -1;

        for(int i = 0; i < str.length(); i++)
        {
            int count_0 = 0, count_1 = 0;

            for(int j = i; j < str.length(); j++)
            {
                if(str[j] == '0')
                {
                    count_0++;
                }
                else
                {
                    count_1++;
                }

                max_diff = max(max_diff, count_0 - count_1);
            }
        }

        if(max_diff < 0)
        {
            cout << "-1" << endl;
        }
        else
        {
            cout << max_diff << endl;
        }
    }
}