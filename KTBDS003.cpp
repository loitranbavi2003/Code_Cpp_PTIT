#include <bits/stdc++.h>

using namespace std;

long long CountWay(int n, int k);

int main()
{
	int test;
	cin >> test;
	while(test--)
	{
		int n, k;
		cin >> n >> k;
		
		long long result = CountWay(n, k);
		cout << result << endl;
	}
}

long long CountWay(int n, int k)
{
	long long dp[n+1][k+1];
	memset(dp, 0, sizeof(dp));
	
	dp[0][0] = 1;
	
	for(int i = 1; i <= n; i++)
	{
		for(int j = 1; j <= k; j++)
		{
			for(int m = 0; m <= 1; m++)
			{
				dp[i][j] += dp[i-1][j-m];
			}
		}
	}
	
	return dp[n][k];
}
