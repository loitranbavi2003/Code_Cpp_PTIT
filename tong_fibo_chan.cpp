#include <bits/stdc++.h>

using namespace std;

long long fibo(long n);

int main()
{
    int test;
    cin >> test;

    while(test--)
    {
        long long N, i = 0, sum = 0;
        cin >> N;

        for(i = 1; i <= N; i++)
        {
            if(fibo(i)%2 == 0)
            {
                // cout << fibo(i) << " ";
                if(fibo(i) <= N)
                {
                    sum += fibo(i);
                }
                else
                {
                    break;
                }
            }
        }
        cout << sum << endl;
    }
}

long long fibo(long n)
{
    long long a = 1, b = 1, c = 0;
    while(n--)
    {
        c = a + b;
        a = b;
        b = c;
    }

    return c;
}