#include <bits/stdc++.h>

using namespace std;

#define MOD 1000000007

long long power(long long a, long long n);

int main()
{
	int test;
	cin >> test;
	while(test--)
	{
		long long n;
		cin >> n;
		
		if(n == 1)
		{
			cout << 5 << endl;
		}
		else
		{
			long long vtriLe = n/2;
			long long vtriChan = n - vtriLe;
			long long result = (power(5, vtriChan)*power(4, vtriLe))%MOD;
			cout << result << endl;
		}
	}
}

long long power(long long a, long long n)
{
	if(n == 1)
	{
		return a;
	}
	else 
	{
		if(n%2 == 0)
		{
			long long half_power = power(a, n/2);
			return (half_power*half_power)%MOD;
		}	
		else
		{
			long long half_power = power(a, n/2);
			long long power = (half_power*half_power)%MOD;
			return (a*power)%MOD;
		}
	}
}
