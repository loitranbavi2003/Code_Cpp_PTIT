#include <bits/stdc++.h>

using namespace std;

int main()
{
    int test;
    cin >> test;
    while(test--)
    {
        int n;
        long double x;
        cin >> n >> x;

        long double mu = x, total = x;

        if(n == 1)
        {
            cout << fixed << setprecision(3) << sqrt(x) << " " << endl;
        }
        else
        {
            for(int i = 2; i <= n; i++)
            {
                total = mu*x + sqrt(total);
                mu = mu*x;
            }

            cout << fixed << setprecision(3) << sqrt(total) << " " << endl;
        }
    }
}