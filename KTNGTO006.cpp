#include <bits/stdc++.h>

using namespace std;

int check_nto(int n);

int main()
{
	int test;
	cin >> test;
	while(test--)
	{
		int n, flag_done = 0;
		cin >> n;
		
		for(int i = n; i >= 1; i--)
		{
			for(int j = 1; j < sqrt(i); j++)
			{
				if(i%j == 0 && check_nto(i/j) == 1)
				{
					flag_done = 1;
					cout << i << endl;
					break;
				}
			}
			
			if(flag_done == 1)
			{
				break;
			}
		}
	}
}

int check_nto(int n)
{
	if(n < 2)
	{
		return 0;
	}
	else if(n > 2)
	{
		if(n%2 == 0)
		{	
			return 0;
		}
		
		for(int i = 3; i <= sqrt(n); i += 2)
		{
			if(n%i == 0)
			{
				return 0;
			}
		}
	}
	
	return 1;
}
