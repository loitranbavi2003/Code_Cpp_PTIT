#include <bits/stdc++.h>

using namespace std;

double isPower(double x, long long n);

int main()
{
    int test;
    cin >> test;
    while(test--)
    {
        double x;
        long long n;

        cin >> x >> n;

        double result = isPower(x, abs(n));
        if(n < 0)
        {
            cout << 1/result << endl;
        }
        else
        {
            cout << result << endl;
        }
    }
}

double isPower(double x, long long n)
{
    if(n == 0)
    {
        return 1;
    }

    if(n == 1)
    {
        return x;
    }

    return x * isPower(x, n - 1);
}