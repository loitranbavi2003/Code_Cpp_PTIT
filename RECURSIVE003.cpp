#include <bits/stdc++.h>

using namespace std;

bool isPowerOfFour(int n);

int main()
{
    int test;
    cin >> test;
    while(test--)
    {
        long long n;
        cin >> n;

        if(isPowerOfFour(n))
        {
            cout << "1" << endl;
        }
        else
        {
            cout << "0" << endl;
        }
    }
}

bool isPowerOfFour(int n)
{
    if(n == 1)
    {
        return true;
    }
    else if(n <= 0 || n%4 != 0)
    {
        return false;
    }

    return isPowerOfFour(n/4);
}