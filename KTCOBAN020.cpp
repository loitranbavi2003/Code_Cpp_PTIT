#include <bits/stdc++.h>

using namespace std;

long long TichCacSo(long long n);
long long FindMax(long long n);

int main()
{
    int test;
    cin >> test;
    while(test--)
    {
        long long n, result = 0;
        cin >> n;

        result = FindMax(n);
        cout << result << endl;
    }
}

long long FindMax(long long n)
{
    long long max = TichCacSo(n);

    string new_number = to_string(n);
    int lenght = new_number.size() - 1;

    while(lenght >= 1)
    {
        if(new_number[lenght] < char(9 + '0'))
        {
            new_number[lenght - 1] -= 1;
            new_number[lenght] = 9 + '0';

            long long number = stoll(new_number);
            long long result = TichCacSo(number);

            if(max < result)
            {
                max = result;
            }
        }
        lenght--;
    }
    return max;
}

long long TichCacSo(long long n)
{
    long long tich = 1;
    while(n != 0)
    {
        tich = tich * (n%10);
        n = n/10;
    }

    return tich;
}