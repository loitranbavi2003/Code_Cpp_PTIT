#include <bits/stdc++.h>

using namespace std;

int main()
{
    int test;
    cin >> test;
    cin.ignore();
    while(test--)
    {
        string str;
        getline(cin, str);

        if(str[0] == '-')
        {
            cout << str.length() - 1 << endl;
        }
        else
        {
            cout << str.length() << endl;
        }
    }
}