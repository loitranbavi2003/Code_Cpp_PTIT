#include <bits/stdc++.h>

using namespace std;

#define MOD 1000000007

int countWays(int N) {
    long long DP[N+1];
    memset(DP, 0, sizeof(DP));
    DP[0] = 1;

    for (int i = 1; i <= N-1; i++) 
    {
        for (int j = i; j <= N; j++) 
        {
            DP[j] += DP[j - i];
            DP[j] %= MOD;
        }
    }

    return DP[N];
}

int main()
{
    int test;
    cin >> test;
    while(test--)
    {
        int n;
        cin >> n;
        int result = countWays(n);
        cout << result << endl;
    }
}