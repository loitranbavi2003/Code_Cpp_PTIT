#include <bits/stdc++.h>

using namespace std;

bool ChuoiDoiXung(string &str);

int main()
{
    int test;
    cin >> test;
    while(test--)
    {
        string str;
        cin >> str;

        int result = ChuoiDoiXung(str);
        cout << result << endl;
    }
}

bool ChuoiDoiXung(string &str)
{
    deque<char> c;

    for(int i = 0; i < str.length()/2; i++)
    {
        if(str[i] != str[str.length() - 1 - i])
        {
            return false;
        }
    }

    return true;
}