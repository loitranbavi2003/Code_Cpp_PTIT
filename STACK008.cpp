#include <bits/stdc++.h>

using namespace std;

int CountStringValid(string &str);

int main()
{
    int test;
    cin >> test;
    cin.ignore();
    while(test--)
    {
        string str;
        cin >> str;

        int count = CountStringValid(str);
        cout << count << endl;
    }
}

int CountStringValid(string &str)
{
    stack<char> s;

    for(int i = 0; i < str.length(); i++)
    {
        if(!s.empty() && (s.top() == '(' && str[i] == ')'))
        {
            s.pop();
        }
        else
        {
            s.push(str[i]);
        }
    }

    return s.size();
}