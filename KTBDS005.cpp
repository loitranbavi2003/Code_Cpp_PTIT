#include <bits/stdc++.h>

using namespace std;

long long CountWay(int n);

int main()
{
	int test;
	cin >> test;
	while(test--)
	{
		int n;
		cin >> n;
		
		long long result = CountWay(n);
		cout << result << endl;
	}
}

long long CountWay(int n)
{
	vector<long long> dp(n+1, 0);
	dp[0] = 1;
	
	int i = 1;
	while(pow(i, 2) <= n)
	{
		int power = pow(i, 2);
		for(int j = power; j <= n; j++)
		{
			dp[j] += dp[j - power];
		}
		
		i++;
	}
	
	return dp[n];
}
