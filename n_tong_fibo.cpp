#include <bits/stdc++.h>

using namespace std;

bool isFibonacci(int n) 
{
    int a = 0, b = 1, c = 1;
    while (c <= n) 
    {
        if (c == n) 
        {
            return true;
        }

        a = b;
        b = c;
        c = a + b;
    }
    return false;
}

int main() {
    int test;
    cin >> test;
    while (test--) 
    {
        int n;
        cin >> n;

        int a = 0, b = n;

        while (a <= n / 2) 
        {
            if (isFibonacci(a) && isFibonacci(b)) 
            {
                if (a + b == n) 
                {
                    cout << a << " " << b << endl;
                    break;
                }
            }
            a++;
            b--;
        }

        if (a > n / 2) 
        {
            cout << "-1" << endl;
        }
    }

    return 0;
}
