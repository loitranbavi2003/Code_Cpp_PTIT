#include <bits/stdc++.h>

using namespace std;

long long ConvertStrToNumber(string str);

int main()
{
    int test;
    cin >> test;
    while(test--)
    {
        int n;
        string str_1, str_2;
        cin >> n >> str_1 >> str_2;

        long long number_1 = 0, number_2 = 0, Max = 0, Min = 0;

        number_1 = ConvertStrToNumber(str_1);
        number_2 = ConvertStrToNumber(str_2);
        Max = max(number_1, number_2);
        Min = min(number_1, number_2);

        cout << Max - Min - 1 << endl;
    }
}

long long ConvertStrToNumber(string str)
{
    int len = str.length();
    long long n = 0;

    for(int i = 0; i < len; i++)
    {
        n += (str[i] - 48) * pow(2, len-1-i);
    }

    return n;
}