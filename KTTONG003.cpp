#include <bits/stdc++.h>

using namespace std;

int main()
{
    int test;
    cin >> test;
    while(test--)
    {
        int a[101][101];
		int m,n;
		cin>>m>>n;
		for(int i=0;i<m;i++)
        {
			for(int j=0;j<n;j++)
            {
				cin>>a[i][j];
			}
		}

		if(m==1)
        {
			long long tong = a[0][0];
			for(int i=1;i<n;i++)
            {
				tong+=a[0][i];
			}
			cout<<tong<<endl;
		}
		else
        {
            for(int i=1;i<n;i++)
            {
                for(int j=0;j<m;j++)
                {
                    int k;
                    if(j==0)
                    {
                        k = max(a[j][i-1],a[j+1][i-1]);
                        a[j][i]+=k;
                    }
                    else if(j<m-1)
                    {
                        k = max(a[j-1][i-1],max(a[j][i-1],a[j+1][i-1]));
                        a[j][i]+=k;
                    }
                    else
                    {
                        k = max(a[j-1][i-1],a[j][i-1]);
                        a[j][i]+=k;
                    }
                }
            }

            int maximum = a[0][n-1];
            for(int i=1;i<m;i++)
            {
                maximum = max(maximum,a[i][n-1]);
            }
            cout<<maximum<<endl;
        }
    }
}