#include <bits/stdc++.h>

using namespace std;

int FirstChar(string &str);

int main()
{
    int test;
    cin >> test;
    cin.ignore();
    while(test--)
    {
        string str;
        cin >> str;

        int result = FirstChar(str);
        cout << result << endl;
    }
}

int FirstChar(string &str)
{
    queue<char> q;
    unordered_map<char, int> charCount;

    for(int i = 0; i < str.length(); i++)
    {
        q.push(str[i]);
        charCount[str[i]]++;
    }

    int index = 0;
    while(!q.empty())
    {
        char temp = q.front();
        q.pop();

        if(charCount[temp] == 1)
        {
            return index;
        }
        index++;
    }

    return -1;
}