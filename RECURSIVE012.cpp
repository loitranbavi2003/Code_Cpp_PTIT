#include <bits/stdc++.h>

using namespace std;

bool chuoidoixung(string &str);

int main()
{
    int test;
    cin >> test;
    cin.ignore();
    while(test--)
    {
        string str;
        cin >> str;

        cout << chuoidoixung(str) << endl;
    }
}

bool chuoidoixung(string &str)
{
    int start = 0, end = str.length() - 1;

    for(int i = start; i <= end/2; i++)
    {
        if(str[i] != str[end - i])
        {
            return false;
        }
    }

    return true;
}