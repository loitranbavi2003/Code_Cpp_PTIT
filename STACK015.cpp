#include <bits/stdc++.h>

using namespace std;

string Delete(string &str, int n);

int main()
{
    int test;
    cin >> test;
    cin.ignore();
    while(test--)
    {
        int n;
        string str;
        cin >> str >> n;

        string result = Delete(str, n);
        cout << result << endl;
    }
}

string Delete(string &str, int n)
{
    stack<char> s;
    int count = 0;

    for(int i = 0; i < str.length(); i++)
    {
        while(n > 0 && !s.empty() && s.top() > str[i])
        {
            n--;
            s.pop();
        }

        s.push(str[i]);
    }

    // Remove remaining k digits from the end if necessary
    while (n > 0) {
        s.pop();
        n--;
    }

    string result;
    while(!s.empty())
    {
        result = s.top() + result;
        s.pop();
    }

    while(result[0] == '0')
    {
        result.erase(0, 1);
    }
    return result.empty() ? "0": result;
}