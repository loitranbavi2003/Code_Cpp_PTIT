#include <bits/stdc++.h>

using namespace std;

unsigned long long BinToDec(string &str);

int main()
{
    int test;
    cin >> test;
    cin.ignore();

    while(test--)
    {
        string str;
        cin >> str;

        unsigned long long result = BinToDec(str);
        cout << result << endl;
    }
}

unsigned long long BinToDec(string &str)
{
    int count_mu = 0;
    unsigned long long sum = 0;

    for (int i = 0; i < str.size(); i++) 
    {
        sum = (sum << 1) + (str[i] - '0');
    }

    return sum;
}