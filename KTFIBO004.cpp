#include <bits/stdc++.h>

using namespace std;

long long Fibo(long long n);
int Check(int n);

int main()
{
	int test;
	cin >> test;
	while(test--)
	{
		int n;
		cin >> n;
		
		if(Check(n))
		{
			cout << 0 << " " << n << endl;
		}
		else
		{
			int flag = 0;
			long long i = 1;
			long long fibo = Fibo(i);
			while(fibo < n)
			{
				if(Check(n - fibo))
				{
					flag = 1;
					break;
				}
				
				i++;
				fibo = Fibo(i);
			}
			
			if(flag == 1)
			{
				cout << fibo << " " << n - fibo << endl;
			}
			else
			{
				cout << -1 << endl;
			}
		}
	}
}

int Check(int n)
{
	long long i = 1;
	long long fibo = Fibo(i);
	while(fibo < n)
	{
		i++;
		fibo = Fibo(i);
	}
	
	if(fibo == n)
	{
		return true;
	}
	else
	{
		return false;
	}
}

long long Fibo(long long n)
{
	if(n == 0 || n == 1)
	{
		return 1;
	}
	
	long long f0 = 1;
	long long f1 = 1;
	long long f2 = 0;
	for(int i = 2; i <= n; i++)
	{
		f2 = f0 + f1;
		f0 = f1;
		f1 = f2;
	}
	
	return f1;
}
