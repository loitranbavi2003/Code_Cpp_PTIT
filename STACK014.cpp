#include <bits/stdc++.h>

using namespace std;

bool isValidString(string &str);

int main()
{
    int test;
    cin >> test;
    cin.ignore();
    while(test--)
    {
        string str;
        cin >> str;

        cout << isValidString(str) << endl;
    }
}

bool isValidString(string &str)
{
    stack<char> s;

    for(int i = 0; i < str.length(); i++)
    {
        if(str[i] == 'a')
        {
            s.push(str[i]);
        }
        else if(!s.empty() && str[i] == 'b' && s.top() == 'a')
        {
            s.pop(); // xoa a o tren
            s.push(str[i]); // nhan ki tu b
        }
        else if(!s.empty() && str[i] == 'c' && s.top() == 'b')
        {
            s.pop(); // xoa b o tren
        }
        else
        {
            return false;
        }
    }

    return s.empty();
}