#include <bits/stdc++.h>

using namespace std;

int main()
{
    int test;
    cin >> test;
    cin.ignore();

    while(test--)
    {
        string time, result;
        getline(cin, time);

        if (time[8] == '?') 
        {
            if (time[9] == '?' || time[9] <= '3') 
            {
                time[8] = '2';
            } 
            else 
            {
                time[8] = '1';
            }
        }
        
        if (time[9] == '?') 
        {
            if (time[8] == '2') 
            {
                time[9] = '3';
            } 
            else 
            {
                time[9] = '9';
            }
        }

        if(time[11] == '?')
        {
            time[11] = '5';
        }
        
        if(time[12] == '?')
        {
            time[12] = '9';
        }

        result = time.substr(7, 7);
        cout << result << endl;
    }
}