#include <bits/stdc++.h>

using namespace std;

vector<long long> fibo(int n);

int main()
{
    int test;
    cin >> test;
    while(test--)
    {
        int n;
        cin >> n;

        vector<long long> arr_fibo = fibo(n);
        cout << arr_fibo[n] << endl;
    }
}

vector<long long> fibo(int n)
{
    vector<long long> arr_fibo;
    arr_fibo.push_back(0);
    arr_fibo.push_back(1);

    for(int i = 2; i <= n; i++)
    {
        arr_fibo.push_back(arr_fibo[i - 1] + arr_fibo[i - 2]);
    }

    return arr_fibo;
}
