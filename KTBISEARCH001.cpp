#include <bits/stdc++.h>

using namespace std;

int main()
{
    int n, m;
    cin >> n >> m;

    vector<int> a(n);
    vector<int> b(m);
    vector<int> vtri(m, 0);
    
    for(int i = 0; i < n; i++)
    {
        cin >> a[i];
    }

    for(int i = 0; i < m; i++)
    {
        cin >> b[i];
    }

    for(int i = 0; i < m; i++)
    {
        int  j = 0;
        while(j < n && b[i] != a[j])
        {
            j++;
        }

        if(j < n && b[i] == a[j])
        {
            vtri[i] = j + 1;
        }
        else
        {
            vtri[i] = 0;
        }
    }

    for(int i = 0; i < m - 1; i++)
    {
        cout << vtri[i] << " ";
    }
    cout << vtri[m-1] << endl;
}