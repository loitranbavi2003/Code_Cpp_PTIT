#include <bits/stdc++.h>

using namespace std;

bool ngoacHopLe(string &str);

int main()
{
    int test;
    cin >> test;
    cin.ignore();
    while(test--)
    {
        string str;
        getline(cin, str);

        cout << ngoacHopLe(str) << endl;
    }
}

bool ngoacHopLe(string &str)
{
    stack<char> s;

    for(int i = 0; i < str.length(); i++)
    {
        if(str[i] == '(' || str[i] == '{' || str[i] == '[')
        {
            s.push(str[i]);
        }
        else 
        {
            if(s.empty())
            {
                return false;
            }
            
            char top = s.top();
            s.pop();
            if(    str[i] == ')' && top != '('
                || str[i] == '}' && top != '{'
                || str[i] == ']' && top != '[')
            {
                return false;
            }
        }
    }

    return s.empty();
}