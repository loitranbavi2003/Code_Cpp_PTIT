#include <bits/stdc++.h>

using namespace std;

int dequy(vector<int> &arr, int index);

int main()
{
    int test;
    cin >> test;
    cin.ignore();
    while(test--)
    {
        string str;
        cin >> str;

        vector<int> arr;
        stringstream ss(str);
        string temp;
        while(getline(ss, temp, ','))
        {
            arr.push_back(stoi(temp));
        }

        int tich = dequy(arr, 0);
        vector<int> result;
        for(int i = 0; i < arr.size(); i++)
        {
            result.push_back(tich/arr[i]);
            if(i < arr.size() - 1)
            {
                cout << result[i] << ",";
            }
        }

        cout << result[arr.size() - 1] << endl;
    }
}

int dequy(vector<int> &arr, int index)
{
    if(index >= arr.size())
    {
        return 1;
    }

    return arr[index] * dequy(arr, index + 1);
}
