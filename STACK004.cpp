#include <bits/stdc++.h>

using namespace std;

string perfectString(string &str);

int main()
{
    int test;
    cin >> test;
    cin.ignore();
    while(test--)
    {
        string str;
        cin >> str;

        string result = perfectString(str);
        cout << result << endl;
    }
}

string perfectString(string &str)
{
    stack<char> s;

    for(int i = 0; i < str.length(); i++)
    {
        if(!s.empty() && abs(str[i] - s.top()) == 32)
        {
            s.pop();
        }
        else
        {
            s.push(str[i]);
        }
    }

    string result;
    while(!s.empty())
    {
        result = s.top() + result;
        s.pop();
    }
    return result;
}
