#include <bits/stdc++.h>

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout.tie(0);

    int t;
    cin >> t;
    while (t--)
    {
        int n;
        float x, S = 0, T = 1;
        unsigned long long M = 1;

        cin >> n >> x;

        for(int i = 1; i <= n; i++)
        {
            T *= x;
            M *= i;
            S += T / M;
        }
        cout << fixed << setprecision(3) << S << " " << endl;
    }
    return 0;
}
