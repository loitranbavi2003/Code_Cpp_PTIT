#include <bits/stdc++.h>

using namespace std;

int main()
{
    int test;
    cin >> test;
    while(test--)
    {
        long long n;
        cin >> n;

        int *arr, i = 0;
        while(n != 0)
        {
            arr[i++] = n%2;
            n = n/2;
        }

        for(int t = i-1; t >= 0; t--)
        {
            cout << arr[t];
        }
        cout << endl;
    }
}