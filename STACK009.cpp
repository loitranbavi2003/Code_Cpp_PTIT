#include <bits/stdc++.h>

using namespace std;

string DecodeString(string &str);

int main()
{
    int test;
    cin >> test;
    cin.ignore();
    while(test--)
    {
        string str;
        cin >> str;

        string result = DecodeString(str);
        cout << result << endl;
    }
}

string DecodeString(string &str)
{
    stack<int> num_stack;
    stack<string> str_stack;

    int number = 0;
    string str_current = "";

    for(int i = 0; i < str.length(); i++)
    {
        if(str[i] >= '0' && str[i] <= '9')
        {
            number = number*10 + (str[i] - '0');
        }
        else if(str[i] == '[')
        {
            num_stack.push(number);
            str_stack.push(str_current);
            number = 0;
            str_current = "";
        }
        else if(str[i] == ']')
        {
            int so_lan = num_stack.top();
            num_stack.pop();

            string temp = str_current;
            for(int i = 1; i < so_lan; i++)
            {
                str_current += temp;
            }

            str_current = str_stack.top() + str_current;
            str_stack.pop();
        }
        else
        {
            str_current += str[i];
        }
    }

    return str_current;
}