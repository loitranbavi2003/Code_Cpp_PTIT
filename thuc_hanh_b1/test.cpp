#include <iostream>
#include <cmath>

using namespace std;

bool is_prime(long long num) 
{
    if (num <= 1) return false;
    if (num <= 3) return true;
    if (num % 2 == 0 || num % 3 == 0) return false;
    
    for (long long i = 5; i * i <= num; i += 6) 
	{
        if (num % i == 0 || num % (i + 2) == 0) return false;
    }
    return true;
}

long long largest_prime_divisor(long long N) 
{
    long long max_prime = -1;
    while (N % 2 == 0) 
	{
        max_prime = 2;
        N /= 2;
    }
    
    for (long long i = 3; i <= sqrt(N); i += 2) 
	{
        while (N % i == 0) {
            max_prime = i;
            N /= i;
        }
    }
    
    if (N > 2) max_prime = N;
    return max_prime;
}

int main() 
{
    int t;
    cin >> t;
    
    while (t--) 
	{
        long long N;
        cin >> N;
        
        long long result = largest_prime_divisor(N);
        cout << result << endl;
    }
    
    return 0;
}

