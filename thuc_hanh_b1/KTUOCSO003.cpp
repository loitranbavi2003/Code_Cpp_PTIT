#include <bits/stdc++.h>

using namespace std;

bool PerfectNumber(long long n);

int main()
{
	int test;
	cin >> test;
	while(test--)
	{
		long long n;
		cin >> n;
		
		if(PerfectNumber(n))
		{
			cout << "1" << endl;
		}
		else
		{
			cout << "0" << endl;
		}
	}
}

bool PerfectNumber(long long n)
{
	long long sum = 1;
	if(n <= 1)
	{
		return false;
	}
	
	for(long i = 2; i*i < n; i++)
	{
		if(n%i == 0)
		{
			sum += i;
			
			if(i*i != n)
			{
				sum += n/i;
			}
		}
		
	}
	
	if(sum == n)
	{
		return true;
	}
	
	return false;
}
