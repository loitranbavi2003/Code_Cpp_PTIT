#include <iostream>
#include <vector>
#include <cmath>

using namespace std;

bool is_prime(long long num) 
{
    if (num <= 1) return false;
    if (num <= 3) return true;
    if (num % 2 == 0 || num % 3 == 0) return false;
    
    for (long long i = 5; i * i <= num; i += 6) 
	{
        if (num % i == 0 || num % (i + 2) == 0) return false;
    }
    return true;
}


int sum_of_largest_prime_divisors(int N) 
{
    int result = 0;
    for (int i = 2; i <= N; i++) 
	{
        if (N%i == 0 && is_prime(i) == true) 
		{
            result = i;
        }
    }
    return result;
}

int main() {
    int t;
    cin >> t;

    while (t--) {
        int N;
        cin >> N;
        
        long sum = 0;
        for(int i = 2; i <= N; i++)
        {
        	sum += sum_of_largest_prime_divisors(i);
		}
		
		cout << sum << endl;
    }

    return 0;
}

