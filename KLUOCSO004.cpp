#include <bits/stdc++.h>

using namespace std;

void result(long long n)
{
    int dem = 0;
    long long m = n;
    long long tich = 1;

    while (n % 2 == 0)
    {
        n /= 2;
        dem++;
    }

    if (dem > 1)
    {
        cout << "YES" << endl;
        return;
    }

    if (dem == 1)
    {
        tich = 2;
    }

    int count;
    for (int i = 3; i <= sqrt(n); i += 2)
    {
        count = 0;
        while (n % i == 0)
        {
            n /= i;
            count++;
        }

        if (count > 1)
        {
            cout << "YES" << endl;
            return;
        }

        if (count == 1)
        {
            tich *= i;
        }
    }

    if (n > 1)
        tich *= n;

    if (tich >= m)
    {
        cout << "NO";
    }
    else
        cout << "YES";
        
    cout << "\n";
}
int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout.tie(0);
    int t;
    cin >> t;
    while (t--)
    {
        long long n;
        cin >> n;
        result(n);
    }
    return 0;
}

// #include<bits/stdc++.h>
// using namespace std;
// // chi can bac cua so nguyen to >1 chac chan nho hon so ban dau
// int main(){
// 	int t;
// 	cin>>t;
// 	while(t--){
// 		unsigned long long n,check=0;
// 		cin>>n;
// 		if(n>1000000000){
// 		long long k=sqrt(n),m=n;
// 		for(long long i=2;i*i*i<=m;i++){
// 			long long v=0;
// 			if(n%i==0){
// 				while(n%i==0){
// 					n/=i;
// 					v++;
// 				}
// 			}
// 			if(v>1){
// 				cout<<"YES"<<endl;
// 				check=1;
// 				break;
// 			}
// 		}
// 		if(check==0){
// 			if(k*k==m) cout<<"YES"<<endl;
// 			else cout<<"NO"<<endl;
// 		}
// 	}
// 	else{
// 		long long k=sqrt(n),m=n;
// 		for(long long i=2;i<=k;i++){
// 			long long v=0;
// 			if(n%i==0){
// 				while(n%i==0){
// 					n/=i;
// 					v++;
// 				}
// 			}
// 			if(v>1){
// 				cout<<"YES"<<endl;
// 				check=1;
// 				break;
// 			}
// 		}
// 		if(check==0){
// 			if(k*k==m) cout<<"YES"<<endl;
// 			else cout<<"NO"<<endl;
// 		}
// 	}
// }
// }