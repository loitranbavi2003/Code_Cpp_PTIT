#include <iostream>
#include <vector>

using namespace std;

// Hàm để đếm số lần xuất hiện của các chữ số từ 0 đến 9
vector<int> countDigits(long long a, long long b) 
{
    vector<int> digitCounts(10, 0); // Khởi tạo mảng đếm các chữ số từ 0 đến 9

    for (long long num = a; num <= b; num++) 
    {
        long long temp = num;
        while (temp > 0) 
        {
            int digit = temp % 10; // Lấy chữ số cuối cùng
            digitCounts[digit]++; // Tăng đếm cho chữ số đó
            temp /= 10; // Loại bỏ chữ số cuối cùng
        }
    }

    return digitCounts;
}

int main() {
    int T;
    cin >> T; // Nhập số lượng bộ test

    while(T--) 
    {
        long long a, b, n, m;
        cin >> a >> b; // Nhập a và b cho từng bộ test

        n = min(a, b);
        m = max(a, b);

        vector<int> result = countDigits(n, m); // Đếm số lần xuất hiện của các chữ số

        // In ra kết quả
        for (int i = 0; i < 10; i++) 
        {
            cout << result[i] << " ";
        }
        cout << endl;
    }

    return 0;
}
