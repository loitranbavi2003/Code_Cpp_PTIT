#include <bits/stdc++.h>

using namespace std;

long long FiboThuN(long long n); // Ham tim so fibo thu n

int main()
{
    int test;
    cin >> test;
    while(test--)
    {
        long long n;
        cin >> n;

        int i = 1;
        long long result = FiboThuN(i);
        while(result < n)
        {
            i++;
            result = FiboThuN(i);
        }

        if(n <= 3)
        {
            cout << 4 << endl;
        }
        else
        {
            if(result - 1 > n)
            {
                cout << n+1 << endl;
            }
            else if((result - 1) == n)
            {
                cout << result+1 << endl;
            }
            else if(result == n)
            {
                cout << result+1 << endl;
            }
        }
    }
}

long long FiboThuN(long long n)
{
    if(n == 1)
    {
        return 1;
    }

    long long a = 1;
    long long b = 1;
    long long c = 0;
    for(long long i = 2; i <= n; i++)
    {
        c = a + b;
        a = b;
        b = c;
    }

    return c;
}