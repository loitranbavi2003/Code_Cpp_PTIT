#include <bits/stdc++.h>

using namespace std;

int hoan_doi_min(string str);
int hoan_doi_max(string str);

int main()
{
    int test;
    cin >> test;
    while(test--)
    {
        string a, b;
        cin >> a >> b;

        int min = 0, max = 0;
        min = hoan_doi_min(a) + hoan_doi_min(b);
        max = hoan_doi_max(a) + hoan_doi_max(b);
        cout << min << " " << max << endl;
    }
}

int hoan_doi_min(string str)
{
    for(int i = 0; i < str.length(); i++)
    {
        if(str[i] == '5')
        {
            str[i] = '3';
        }
    }

    return stoi(str);
}

int hoan_doi_max(string str)
{
    for(int i = 0; i < str.length(); i++)
    {
        if(str[i] == '3')
        {
            str[i] = '5';
        }
    }

    return stoi(str);
}