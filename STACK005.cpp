#include <bits/stdc++.h>

using namespace std;

int LengthStringMin(string &str);

int main()
{
    int test;
    cin >> test;
    cin.ignore();
    while(test--)
    {
        string str;
        cin >> str;

        int length = LengthStringMin(str);
        cout << length << endl;
    }
}

int LengthStringMin(string &str)
{
    stack<char> s;

    for(int i = 0; i < str.length(); i++)
    {
        if(!s.empty() && ((s.top() == 'A' && str[i] == 'B') || (s.top() == 'C' && str[i] == 'D')))
        {
            s.pop();
        }
        else
        {
            s.push(str[i]);
        }
    }

    return s.size();
}