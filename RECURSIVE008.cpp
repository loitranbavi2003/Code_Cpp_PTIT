#include <bits/stdc++.h>

using namespace std;

int number_prime(int n);

int main()
{
    int test;
    cin >> test;
    while(test--)
    {
        int a, b;
        cin >> a >> b;

        int sum = 0;
        for(int i = a; i <= b; i++)
        {
            if(number_prime(i))
            {
                sum += i;
            }
        }

        cout << sum << endl;
    }
}

int number_prime(int n)
{
    if(n < 2)
    {
        return 0;
    }
    else if(n > 2)
    {
        if(n%2 == 0)
        {
            return 0;
        }

        for(int i = 3; i <= sqrt(n); i += 2)
        {
            if(n%i == 0)
            {
                return 0;
            }
        }
    }

    return 1;
}