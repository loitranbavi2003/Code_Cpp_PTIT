#include <bits/stdc++.h>

using namespace std;

int main()
{
    int test;
    cin >> test;
    
    while(test--)
    {
        int arr[10] = {0};
        // memset(arr, 0, sizeof(arr));

        long a, b, Max = 0, Min = 0;
        cin >> a >> b;

        Max = max(a, b);
        Min = min(a, b);
        a = Min;
        b = Max;

        for(long i = a; i <= b; i++)
        {
            if(i <= 9)
            {
                arr[i]++;
            }
            else
            {
                long x = i;
                while(x != 0)
                {
                    int y = x%10;
                    arr[y]++;
                    x /= 10;
                }
            }
        }

        for(int i = 0; i <= 9; i++)
        {
            cout << arr[i] << " ";
        }
        cout << endl;
    }
}



// #include <bits/stdc++.h>

// using namespace std;

// vector<int> CountDigits(long number);

// int main()
// {
//     ios_base::sync_with_stdio(false);
//     cin.tie(0);
//     cout.tie(0);
//     long test;
//     cin >> test;

//     while(test--)
//     {
//         vector<int> digitCounts(10, 0);
//         long a, b, Max = 0, Min = 0;
//         cin >> a >> b;

//         Max = max(a, b);
//         Min = min(a, b);
//         a = Min;
//         b = Max;

//         for(long i = a; i <= b; i++)
//         {
//             vector<int> counts = CountDigits(i);

//             for(int j = 0; j <= 9; j++)
//             {
//                 digitCounts[j] += counts[j];
//             }
//         }

//         for(int i = 0; i <= 9; i++)
//         {
//             cout << digitCounts[i];
//             if(i < 9)
//             {
//                 cout << " ";
//             }
//         }
//         cout << endl;
//     }
// }

// vector<int> CountDigits(long number)
// {
//     vector<int> count(10, 0);

//     while(number != 0)
//     {
//         int x = number%10;
//         count[x]++;
//         number /= 10;
//     }

//     return count;
// }